from bson.objectid import ObjectId
from pydantic import BaseModel
from typing import List, Optional
from jwtdown_fastapi.authentication import Token


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value: ObjectId | str) -> ObjectId:
        if value:
            try:
                ObjectId(value)
            except:
                raise ValueError(f"Not a valid object id: {value}")
        return value


class SessionOut(BaseModel):
    jti: str
    account_id: str


class AccountIn(BaseModel):
    email: str
    password: str
    full_name: str


class Account(AccountIn):
    id: PydanticObjectId


class AccountOut(BaseModel):
    id: str
    email: str
    full_name: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


class AccountUpdateIn(BaseModel):
    email: Optional[str]
    password: Optional[str]
    full_name: Optional[str]


class PlayListsIn(BaseModel):
    email: str
    playlist_name: str
    playlist_data: dict


class PlayListOut(BaseModel):
    playlist_name: str
    playlist_data: dict


class PlayListUpdate(BaseModel):
    email: Optional[str]
    playlist_name: Optional[str]
    playlist_data: Optional[dict]
