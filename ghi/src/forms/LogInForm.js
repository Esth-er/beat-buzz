import '../App.css';
import React, { useState } from 'react';
import { NavLink, useNavigate } from "react-router-dom";
import { useToken } from "../UseToken"



function BootstrapInput(props) {
    const { id, placeholder, labelText, value, onChange, type } = props;

    return (
        <div className="mb-3">
            <label htmlFor={id} className="form-label">{labelText}</label>
            <input value={value} onChange={onChange} required type={type} className="form-control" id={id} placeholder={placeholder}/>
        </div>
    )
}

function LogInForm(props) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [token, login] = useToken();
    const navigate = useNavigate();
    const saved = localStorage.getItem("token");
    const initialValue = JSON.parse(saved);

    const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await login(email, password);
      setEmail("");
      setPassword("");
      //initialValue || "";

    } catch (error) {
      console.log(error);
      console.log("Wrong email or password!");
    }
    navigate('/')
  };


    return (

        <div className="container">

        <form onSubmit={handleSubmit}>
            <BootstrapInput
                id="email"
                placeholder="you@example.com"
                labelText="Your email address"
                value={email}
                onChange={e => setEmail(e.target.value)}
                type="email" />
            <BootstrapInput
                id="password"
                placeholder="Super secret password"
                labelText="Password"
                value={password}
                onChange={e => setPassword(e.target.value)}
                type="password" />
            <button type="submit" className="btn btn-primary">LOGIN</button>
        </form>
        <form className="login-form" onSubmit={handleSubmit}>
        <div className="create-account-container">
            <p><em>Don't have an account yet?</em></p>
            <NavLink className="btn btn-primary" id="" aria-current="page" to="/account/create/">SIGN UP</NavLink>
        </div>
        </form>

        <div className="loader-container">
			<div className="rectangle-1"></div>
			<div className="rectangle-2"></div>
			<div className="rectangle-3"></div>
			<div className="rectangle-4"></div>
			<div className="rectangle-5"></div>
			<div className="rectangle-6"></div>
			<div className="rectangle-5"></div>
			<div className="rectangle-4"></div>
			<div className="rectangle-3"></div>
			<div className="rectangle-2"></div>
			<div className="rectangle-1"></div>
		</div>

    <div/>
    </div>
    );
}







export default LogInForm;
