import { BrowserRouter, Routes, Route } from 'react-router-dom';
// import { useEffect, useState } from 'react';
import { AuthProvider, useToken } from './UseToken';
// import ErrorNotification from './ErrorNotification';
import MainPage from './MainPage';
import Nav from './Nav';
import AccountForm from './forms/AccountForm';
import LogInForm from './forms/LogInForm';
import Logout from './LogOut';
import Account from './AccountPage';
import Playlists from './PlaylistsPage';
import Explore from "./explore";
import SearchView from './Search';
import NewReleases from './MusicPage';

function GetToken() {
  // Get token from JWT cookie (if already logged in)
  useToken();
  return null;
}

function App() {
  return (
    <>
      <BrowserRouter>
        <Nav />
        <AuthProvider>
          <GetToken />
          <div className="container">
            <Routes>
              <Route path="/" element={<MainPage />} />
              <Route path="account">
                <Route path="login" element={<LogInForm />} />
                <Route path="create" element={<AccountForm />} />
                <Route path="logout" element={<Logout />} />
                <Route path="profile" element={<Account />} />
              </Route>
              <Route path="/explorepage" element={<Explore />} />
              <Route path="/search" element={<SearchView />} />
              <Route path="playlists" element={<Playlists />} />
              <Route path="/music" element={<NewReleases />} />
            </Routes>
          </div>
        </AuthProvider>
      </BrowserRouter>
    </>
  );
}

export default App;
