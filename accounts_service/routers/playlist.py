from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from models import HttpError, PlayListsIn, PlayListOut, PlayListUpdate
from pydantic import BaseModel
from queries.playlist_queries import PlaylistQueries, DuplicatePlaylistError


router = APIRouter()


@router.get("/api/playlists/", response_model=list[PlayListOut])
async def get_playlists(repo: PlaylistQueries = Depends()):
    return repo.get_all()


@router.post("/api/playlists/", response_model=PlayListOut)
async def create_playlists(
    info: PlayListsIn,
    repo: PlaylistQueries = Depends(),
):
    try:
        account = repo.create(info)
    except DuplicatePlaylistError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="This playlist name already exists",
        )
    return account


@router.delete("/api/playlists/{playlist_name}", response_model=bool)
async def delete_playlists(
    email: str,
    playlist_name: str,
    repo: PlaylistQueries = Depends(),
):
    deletion = repo.delete(email=email, playlist_name=playlist_name)
    return deletion


@router.put("/api/playlists/{playlist_name}", response_model=PlayListOut)
async def update_playlists(
    email: str,
    playlist_name: str,
    update_info: PlayListUpdate,
    repo: PlaylistQueries = Depends(),
):
    playlist = repo.update(
        email=email, playlist_name=playlist_name, info=update_info
    )
    return playlist
