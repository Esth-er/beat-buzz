import React, { useState, useEffect } from 'react';
import { Container, Row, Col, ListGroup, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const PlaylistPage = () => {
  const [playlists, setPlaylists] = useState([]);

  useEffect(() => {
    fetch('https://your-api.com/playlists')
      .then(res => res.json())
      .then(data => setPlaylists(data))
      .catch(err => console.log(err));
  }, []);

  return (
    <Container>
      <Row>
        <Col>
          {playlists.length > 0 ? (
            <ListGroup>
              {playlists.map(playlist => (
                <ListGroup.Item key={playlist.id}>{playlist.name}</ListGroup.Item>
              ))}
            </ListGroup>
          ) : (
            <>
            <div className="noplaylists">
              <p>You have no playlists.</p></div>
              <Link to="/explorepage">
                <Button variant="primary">Start adding to your playlists now</Button>
              </Link>
            </>
          )}
        </Col>
      </Row>
    </Container>
  );
};

export default PlaylistPage;
