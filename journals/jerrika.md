## January 27
last minute code checking for bugs and errors.

## January 26
Continuted working on unit testing and finished unit test for 'get users'
changed a few css properties

## January 25
started looking into and worknig on unit testing

## 24 January
Faced merge conflicts again and rebasing errors.
individual pushes were being pushed into other people's branches.
Worked with Esther on trying to figure out why my branch continued to have a rebase error.

## 23 January
Added an account // "profile" page for when users are logged in.
When they are not logged in it will say please sign into your account
and have a form to sign in.
when signed in you should be able to see your profile,
however currently i cant get it to work even when logged in.
Added playlist page
When user has laylists created it will show a list of their playlists,
if there are no playlists it will display a message:
"you have no playlists"
and theres a button that redirects you to the explore page.

## 22 January
Worked on CSS for the login page and sign up page.
added some small effect and details
e.g the shape of the form, the buttons, the animation at the bottom
etc.

## 20 January
Got the homepage fully rendered, and nav bar is working.
Fixed the cursor issue and now there is a cursor on all pages and screens!

added a few more design featurs to the main page.
Tried adding other featurs from codepen but it wouldnt work with bootstrap.
Spent A LOT of time trying to get magic text on the main page but it wouldn't work. :')
## 19 January
could not get CSS to work and render after pulling from main.
decided instead of fighting merge conflicts to just pull from main and start CSS from scratch.
## 18 January
more issues when pulling from git,
had many merge conflicts, thought i resolved them corrently however i did not.
## 17 January
tried working on the frontend for login page, but the code i was using wouldnt work with bootstrap.
scrapped the idea.

## 13 Jenuary. 2023
worked with esther trying to get homepage theme to function.
Cursor not showing up on any pages except Main page.
Tried to get homepage theme to work, sucessfully got it to render but the nav bar and homepage was a little buggy.

## Januarty 12, 2023
Having issues pulling from other branches,
keep getting error message
"error: Your local changes to the following files would be overwritten by checkout:
        journals/jerrika.md
Please commit your changes or stash them before you switch branches.
Aborting"
even though i did make commits. Figured it out.

Added a new nav bar with CSS and React components.
Also created a design for the homepage and redesigned the login form.

All code was working and rendering untill pushed. Stopped working and rendering. Just got a white screen.
Waled away. Will try again tomorrow.


## January 10, 2023
Still trying to figure out how we're going to make this website look.
Can't decide on design or layout.

## January 9, 2023
Assisted Esther with Nav.js bug. Couldn't figure it out :')

## January 7 & 8, 2023
Spent the weekend looking into designs and features using react that could be implmented into our app.


## January 6, 2023
Paired with Esther
Added all 'Account' queries code in account_queries.py
created playlist and user databases in the queries directory
created all queries for those 2 databases
user routers added.... have tested to see if functional though.
not sure how authentication works with mongo db...

AH moment of the day was when I finally realised what "accountIn" / "accountOut" actually meants and did.


## January 4, 2023
Standup with group - tried to figure out work distribution.
Did some paired programming with Esther,
had a few issues setting up the database and errors with docker containers.


## January 3, 2023
Today I worked on small things:
Check to make sure our API was working. It does.
I cloned down the repository for the project.
Had an end of the day stand up with my team.
Going to further read through the Project advice on Learn.

## 22 December, 2022
Updated wireframing per instructor's recommendations.


## 21 December, 2022
Worked on wireframing and discussed api's with teammates.
