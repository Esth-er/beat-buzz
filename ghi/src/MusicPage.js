import React, { useEffect, useState } from "react";

function NewReleases() {
  const [releasesData, setReleasesData] = useState([]);

  useEffect(() => {
    async function getData() {
      const url = `${process.env.REACT_APP_BEATBUZZ_MUSIC_API_HOST}/api/browse/new-releases`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const selectedData = data["albums"]["items"].slice(0, 15); // limits how many post we need
        console.log(selectedData);
        setReleasesData(selectedData);
      }
    }

    getData();
  }, [setReleasesData]);

  return (
    <div>
      <div className="container text-center">
        <h2 className="text-center">New Releases</h2>
        <div className="row align-items-start">
          {releasesData.map((album) => {
            return (
              <div className="col mb-4" key={album.id}>
                <div className="card" style={{ height: "fit-content", width: "21rem" }}>
                  <img src={album.images[0].url} className="card-img-top" alt={album.description} style={{ height: "19.5rem", width: "21rem" }} />
                  <div className="card-body">
                    <h6 className="card-title">
                      <b>{album.name}</b>
                    </h6>
                    <p className="card-text">{album.artists[0].name}</p>
                    <p className="card-text">{album.release_date}</p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default NewReleases;
